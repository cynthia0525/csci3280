#pragma once
#pragma comment(lib, "Ws2_32.lib")

#include <stdlib.h>
#include <Windows.h>
#include <iostream>
#include <mutex>
#include <list>

using namespace std;

#define PORT_NUM (3280)

class server {
public:
	// IP & socket list
	typedef struct terminal {
		SOCKET sockfd;
		char ip[16];
	} node;
	list <node*> peerList;

	//設定 socket
	SOCKET sConnect;
	int err;
	mutex mtx;
	int serverOpen;
	int initi;

	server() {
		//開始 Winsock-DLL
		WSADATA wsaData;
		WORD DLLVersion = MAKEWORD(2, 1);
		err = WSAStartup(DLLVersion, &wsaData);

		//宣告給 socket 使用的 sockadder_in 結構
		SOCKADDR_IN addr;
		memset((char*)&addr, 0, sizeof(addr));
		addr.sin_addr.s_addr = INADDR_ANY;
		addr.sin_family = AF_INET;
		addr.sin_port = htons(PORT_NUM);

		//設定 Listen
		sConnect = socket(AF_INET, SOCK_STREAM, NULL);
		if (sConnect == INVALID_SOCKET) {
			err = WSAGetLastError();
			WSACleanup();
			return;
		}

		// Bind the socket
		int iResult = ::bind(sConnect, (SOCKADDR*)&addr, sizeof(addr));
		if (iResult == SOCKET_ERROR) {
			err = WSAGetLastError();
			closesocket(sConnect);
			WSACleanup();
			return;
		}

		//SOMAXCONN: listening without any limit
		if (listen(sConnect, SOMAXCONN) == SOCKET_ERROR) {
			err = WSAGetLastError();
			closesocket(sConnect);
			WSACleanup();
			return;
		}

		err = WSAGetLastError();
		serverOpen = 1;
		initi = 0;
	}

	~server() {
		//若之後不再使用，可用 closesocket 關閉連線
		serverOpen = 0;
		closesocket(sConnect);
		WSACleanup();
	};

	void serverWorking() {
		while (serverOpen) {
			acceptConnection();
		}
	}

	void createPeer(SOCKET sockfd, char* ipaddr) {
		if (initi == 1) {
			initi++;
			return;
		}

		node *newTerminal = new node();
		newTerminal->sockfd = sockfd;
		strcpy(newTerminal->ip, ipaddr);
		peerList.push_front(newTerminal);
		initi++;
	}

	void acceptConnection() {
		SOCKADDR_IN clientAddr;
		int addrlen = sizeof(SOCKADDR_IN);

		SOCKET sockfd = accept(sConnect, (SOCKADDR*)&clientAddr, &addrlen);
		err = WSAGetLastError();
		if (err > 0) return;

		createPeer(sockfd, inet_ntoa(clientAddr.sin_addr));
		for (int i = 0; i < peerList.size() - 1; i++) {
			char* temp = (char*)malloc(sizeof(char) * 17);
			strcpy(temp, (peerList.back() + i)->ip);
			strcat(temp, "\n");
			sendData(sockfd, temp);
			free(temp);
		}

		char* temp = (char*)malloc(sizeof(char) * 17);
		strcpy(temp, peerList.front()->ip);
		strcat(temp, "\n");
		for (const auto& peer : peerList) {
			sendData(peer->sockfd, temp);
		}
		free(temp);
	}

	void sendData(SOCKET sockfd, char *message) {
		//傳送 server 端的訊息
		int r = send(sockfd, message, strlen(message), 0);
		if (r == SOCKET_ERROR) {
			err = WSAGetLastError();
			closesocket(sockfd);
			WSACleanup();
			return;
		}
		cout << "sent " << message << endl;
	}
};