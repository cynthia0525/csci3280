/********************************************************************************
** Form generated from reading UI file 'P2PKaraokeSystemp15928.ui'
**
** Created by: Qt User Interface Compiler version 5.7.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef P2PKARAOKESYSTEMP15928_H
#define P2PKARAOKESYSTEMP15928_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QScrollArea>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QProgressBar *progressBar;
    QPushButton *browseButton;
    QPushButton *playButton;
    QPushButton *nextSong;
    QPushButton *prevSong;
    QLabel *nowPlaying;
    QScrollArea *songList;
    QWidget *scrollAreaWidgetContents;
    QTableWidget *tableWidget;
    QTextEdit *searchField;
    QLabel *lyricBox;
    QLabel *searchLabel;
    QPushButton *connectButton;
    QLabel *lyricLabel;
    QPushButton *addFile;
    QPushButton *deleteFile;
    QRadioButton *autoplay;
    QRadioButton *shuffle;
    QPushButton *editButton;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(1280, 720);
        QFont font;
        font.setFamily(QStringLiteral("Agency FB"));
        font.setPointSize(6);
        MainWindow->setFont(font);
        MainWindow->setWindowOpacity(1);
        MainWindow->setToolButtonStyle(Qt::ToolButtonIconOnly);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        progressBar = new QProgressBar(centralWidget);
        progressBar->setObjectName(QStringLiteral("progressBar"));
        progressBar->setGeometry(QRect(20, 675, 880, 25));
        progressBar->setValue(0);
        progressBar->setAlignment(Qt::AlignJustify|Qt::AlignVCenter);
        progressBar->setTextVisible(false);
        browseButton = new QPushButton(centralWidget);
        browseButton->setObjectName(QStringLiteral("browseButton"));
        browseButton->setGeometry(QRect(970, 120, 280, 50));
        QFont font1;
        font1.setFamily(QStringLiteral("Arial"));
        font1.setPointSize(12);
        font1.setBold(true);
        font1.setWeight(75);
        browseButton->setFont(font1);
        browseButton->setCursor(QCursor(Qt::PointingHandCursor));
        playButton = new QPushButton(centralWidget);
        playButton->setObjectName(QStringLiteral("playButton"));
        playButton->setGeometry(QRect(400, 550, 100, 100));
        playButton->setCursor(QCursor(Qt::PointingHandCursor));
        playButton->setMouseTracking(false);
        playButton->setAutoFillBackground(true);
        QIcon icon;
        icon.addFile(QStringLiteral("play.png"), QSize(), QIcon::Normal, QIcon::Off);
        playButton->setIcon(icon);
        playButton->setIconSize(QSize(100, 100));
        playButton->setCheckable(false);
        nextSong = new QPushButton(centralWidget);
        nextSong->setObjectName(QStringLiteral("nextSong"));
        nextSong->setGeometry(QRect(550, 550, 100, 100));
        nextSong->setCursor(QCursor(Qt::PointingHandCursor));
        nextSong->setAutoFillBackground(true);
        QIcon icon1;
        icon1.addFile(QStringLiteral("next.png"), QSize(), QIcon::Normal, QIcon::Off);
        nextSong->setIcon(icon1);
        nextSong->setIconSize(QSize(100, 100));
        prevSong = new QPushButton(centralWidget);
        prevSong->setObjectName(QStringLiteral("prevSong"));
        prevSong->setGeometry(QRect(250, 550, 100, 100));
        prevSong->setCursor(QCursor(Qt::PointingHandCursor));
        prevSong->setAutoFillBackground(true);
        QIcon icon2;
        icon2.addFile(QStringLiteral("prev.png"), QSize(), QIcon::Normal, QIcon::Off);
        prevSong->setIcon(icon2);
        prevSong->setIconSize(QSize(100, 100));
        nowPlaying = new QLabel(centralWidget);
        nowPlaying->setObjectName(QStringLiteral("nowPlaying"));
        nowPlaying->setGeometry(QRect(970, 185, 280, 40));
        QFont font2;
        font2.setFamily(QStringLiteral("Arial"));
        font2.setPointSize(12);
        nowPlaying->setFont(font2);
        nowPlaying->setStyleSheet(QLatin1String("background-color: rgb(255, 255, 255);\n"
"border: 1px solid black;"));
        songList = new QScrollArea(centralWidget);
        songList->setObjectName(QStringLiteral("songList"));
        songList->setGeometry(QRect(20, 90, 880, 435));
        songList->setStyleSheet(QLatin1String("background-color: rgb(255, 255, 255);\n"
"border: 1px solid black;"));
        songList->setWidgetResizable(true);
        scrollAreaWidgetContents = new QWidget();
        scrollAreaWidgetContents->setObjectName(QStringLiteral("scrollAreaWidgetContents"));
        scrollAreaWidgetContents->setGeometry(QRect(0, 0, 878, 433));
        tableWidget = new QTableWidget(scrollAreaWidgetContents);
        if (tableWidget->columnCount() < 4)
            tableWidget->setColumnCount(4);
        QTableWidgetItem *__qtablewidgetitem = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(0, __qtablewidgetitem);
        QTableWidgetItem *__qtablewidgetitem1 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(1, __qtablewidgetitem1);
        QTableWidgetItem *__qtablewidgetitem2 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(2, __qtablewidgetitem2);
        QTableWidgetItem *__qtablewidgetitem3 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(3, __qtablewidgetitem3);
        tableWidget->setObjectName(QStringLiteral("tableWidget"));
        tableWidget->setGeometry(QRect(0, 0, 878, 433));
        QFont font3;
        font3.setPointSize(12);
        tableWidget->setFont(font3);
        tableWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);
        tableWidget->setSortingEnabled(true);
        tableWidget->horizontalHeader()->setDefaultSectionSize(280);
        tableWidget->horizontalHeader()->setMinimumSectionSize(100);
        tableWidget->verticalHeader()->setVisible(false);
        songList->setWidget(scrollAreaWidgetContents);
        searchField = new QTextEdit(centralWidget);
        searchField->setObjectName(QStringLiteral("searchField"));
        searchField->setGeometry(QRect(125, 30, 725, 40));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(searchField->sizePolicy().hasHeightForWidth());
        searchField->setSizePolicy(sizePolicy);
        searchField->setFont(font2);
        searchField->viewport()->setProperty("cursor", QVariant(QCursor(Qt::IBeamCursor)));
        searchField->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
        searchField->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
        lyricBox = new QLabel(centralWidget);
        lyricBox->setObjectName(QStringLiteral("lyricBox"));
        lyricBox->setGeometry(QRect(970, 545, 280, 150));
        lyricBox->setFont(font2);
        lyricBox->setStyleSheet(QLatin1String("background-color: rgb(255, 255, 255);\n"
"border: 1px solid black;"));
        lyricBox->setAlignment(Qt::AlignCenter);
        lyricBox->setWordWrap(true);
        lyricBox->setMargin(10);
        searchLabel = new QLabel(centralWidget);
        searchLabel->setObjectName(QStringLiteral("searchLabel"));
        searchLabel->setGeometry(QRect(20, 30, 100, 40));
        searchLabel->setFont(font2);
        searchLabel->setLayoutDirection(Qt::LeftToRight);
        searchLabel->setAlignment(Qt::AlignCenter);
        connectButton = new QPushButton(centralWidget);
        connectButton->setObjectName(QStringLiteral("connectButton"));
        connectButton->setGeometry(QRect(970, 25, 280, 50));
        connectButton->setFont(font2);
        connectButton->setCursor(QCursor(Qt::PointingHandCursor));
        lyricLabel = new QLabel(centralWidget);
        lyricLabel->setObjectName(QStringLiteral("lyricLabel"));
        lyricLabel->setGeometry(QRect(970, 505, 75, 40));
        lyricLabel->setFont(font2);
        lyricLabel->setLayoutDirection(Qt::LeftToRight);
        lyricLabel->setAlignment(Qt::AlignCenter);
        addFile = new QPushButton(centralWidget);
        addFile->setObjectName(QStringLiteral("addFile"));
        addFile->setGeometry(QRect(990, 250, 100, 100));
        addFile->setCursor(QCursor(Qt::PointingHandCursor));
        QIcon icon3;
        icon3.addFile(QStringLiteral("add.png"), QSize(), QIcon::Normal, QIcon::Off);
        addFile->setIcon(icon3);
        addFile->setIconSize(QSize(100, 100));
        deleteFile = new QPushButton(centralWidget);
        deleteFile->setObjectName(QStringLiteral("deleteFile"));
        deleteFile->setGeometry(QRect(1130, 250, 100, 100));
        deleteFile->setCursor(QCursor(Qt::PointingHandCursor));
        QIcon icon4;
        icon4.addFile(QStringLiteral("delete.png"), QSize(), QIcon::Normal, QIcon::Off);
        deleteFile->setIcon(icon4);
        deleteFile->setIconSize(QSize(100, 100));
        autoplay = new QRadioButton(centralWidget);
        autoplay->setObjectName(QStringLiteral("autoplay"));
        autoplay->setGeometry(QRect(1120, 400, 120, 30));
        autoplay->setFont(font2);
        autoplay->setCursor(QCursor(Qt::PointingHandCursor));
        autoplay->setAutoExclusive(false);
        shuffle = new QRadioButton(centralWidget);
        shuffle->setObjectName(QStringLiteral("shuffle"));
        shuffle->setGeometry(QRect(1120, 450, 120, 30));
        shuffle->setFont(font2);
        shuffle->setCursor(QCursor(Qt::PointingHandCursor));
        shuffle->setAutoExclusive(false);
        editButton = new QPushButton(centralWidget);
        editButton->setObjectName(QStringLiteral("editButton"));
        editButton->setGeometry(QRect(990, 390, 100, 100));
        editButton->setFont(font2);
        editButton->setCursor(QCursor(Qt::PointingHandCursor));
        QIcon icon5;
        icon5.addFile(QStringLiteral("edit.png"), QSize(), QIcon::Normal, QIcon::Off);
        editButton->setIcon(icon5);
        editButton->setIconSize(QSize(100, 100));
        MainWindow->setCentralWidget(centralWidget);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "Music Player", Q_NULLPTR));
        browseButton->setText(QApplication::translate("MainWindow", "Browse Database", Q_NULLPTR));
        playButton->setText(QString());
        nextSong->setText(QString());
        prevSong->setText(QString());
        nowPlaying->setText(QString());
        QTableWidgetItem *___qtablewidgetitem = tableWidget->horizontalHeaderItem(0);
        ___qtablewidgetitem->setText(QApplication::translate("MainWindow", "wavPath", Q_NULLPTR));
        QTableWidgetItem *___qtablewidgetitem1 = tableWidget->horizontalHeaderItem(1);
        ___qtablewidgetitem1->setText(QApplication::translate("MainWindow", "Song", Q_NULLPTR));
        QTableWidgetItem *___qtablewidgetitem2 = tableWidget->horizontalHeaderItem(2);
        ___qtablewidgetitem2->setText(QApplication::translate("MainWindow", "Artist", Q_NULLPTR));
        QTableWidgetItem *___qtablewidgetitem3 = tableWidget->horizontalHeaderItem(3);
        ___qtablewidgetitem3->setText(QApplication::translate("MainWindow", "Album", Q_NULLPTR));
        searchField->setHtml(QApplication::translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Arial'; font-size:12pt; font-weight:400; font-style:normal;\">\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><br /></p></body></html>", Q_NULLPTR));
        lyricBox->setText(QString());
        searchLabel->setText(QApplication::translate("MainWindow", "Search", Q_NULLPTR));
        connectButton->setText(QApplication::translate("MainWindow", "Start P2P Connection", Q_NULLPTR));
        lyricLabel->setText(QApplication::translate("MainWindow", "Lyric", Q_NULLPTR));
        addFile->setText(QString());
        deleteFile->setText(QString());
        autoplay->setText(QApplication::translate("MainWindow", "autoplay", Q_NULLPTR));
        shuffle->setText(QApplication::translate("MainWindow", "shuffle", Q_NULLPTR));
        editButton->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // P2PKARAOKESYSTEMP15928_H
