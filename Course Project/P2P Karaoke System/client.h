#pragma once
#pragma comment(lib, "Ws2_32.lib")

#include <WinSock2.h>
#include <iostream>
#include <mutex>

using namespace std;

#define PORT_NUM (3280)
#define BUF_SIZE 4096

class client {
public:
	//設定 socket
	SOCKET sConnect;
	int err;
	mutex mtx;
	char message[BUF_SIZE] = { 0 };

	client(const char *IPtoConnect) {
		//開始 Winsock-DLL
		WSAData wsaData;
		WORD DLLVersion = MAKEWORD(2, 1);
		int r = WSAStartup(DLLVersion, &wsaData);

		//AF_INET: internet-family
		//SOCKET_STREAM: connection-oriented socket
		sConnect = socket(AF_INET, SOCK_STREAM, NULL);

		//宣告給 socket 使用的 sockadder_in 結構
		SOCKADDR_IN addr;
		memset((char*)&addr, 0, sizeof(addr));
		addr.sin_addr.s_addr = inet_addr(IPtoConnect);
		addr.sin_family = AF_INET;
		addr.sin_port = htons(PORT_NUM);
		connect(sConnect, (SOCKADDR*)&addr, sizeof(addr));
		err = WSAGetLastError();
	}

	~client() {
		//若之後不再使用，可用 closesocket 關閉連線
		closesocket(sConnect);
		WSACleanup();
	};

	void receiveData() {
		// wait for mtx
		while (!mtx.try_lock());

		//接收 server 端的訊息
		int r = recv(sConnect, message, BUF_SIZE, 0);
		if (r == SOCKET_ERROR) {
			err = WSAGetLastError();
			closesocket(sConnect);
			WSACleanup();
			return;
		}
		cout << "received " << message << endl;

		// release mtx
		mtx.unlock();
	}
};
