#include "P2PKaraokeSystem.h"
#include <stdlib.h>
#include <iostream>
#include <sstream>
#include <thread>
#include <QSignalMapper>
#include <QFileDialog>
#include <QInputDialog>
#include <QMessageBox>
#include <QHBoxLayout>
#include <QComboBox>
#include <QTextStream>

client *p2pClient;
thread clientThread;

server *p2pServer;
thread serverThread;

QString database;
QString entry;
QList<QTimer*> lyricTimer;

P2PKaraokeSystem::P2PKaraokeSystem(QWidget *parent)
	: QMainWindow(parent)
{
	ui->setupUi(this);
	ui->tableWidget->setColumnHidden(0, true);

	// !connects
	connect(ui->browseButton, SIGNAL(clicked()), this, SLOT(loadDatabase()));
	connect(ui->addFile, SIGNAL(clicked()), this, SLOT(addSong()));
	connect(ui->deleteFile, SIGNAL(clicked()), this, SLOT(popupSelector()));
	connect(ui->editButton, SIGNAL(clicked()), this, SLOT(popupSelector()));
	connect(ui->tableWidget, SIGNAL(itemDoubleClicked(QTableWidgetItem*)), this, SLOT(playSong(QTableWidgetItem*)));
	connect(ui->playButton, SIGNAL(clicked()), this, SLOT(togglePlay()));
	connect(ui->prevSong, SIGNAL(clicked()), this, SLOT(playPrev()));
	connect(ui->nextSong, SIGNAL(clicked()), this, SLOT(playNext()));
	connect(timer, SIGNAL(timeout()), this, SLOT(updateProgress()));
	connect(ui->searchField, SIGNAL(textChanged()), this, SLOT(searchSong()));
	connect(ui->connectButton, SIGNAL(clicked()), this, SLOT(toggleConnect()));
}

// update selection
void P2PKaraokeSystem::updateSelection(int index) {
	QComboBox *selector = qobject_cast<QComboBox *>(sender());
	selector->blockSignals(true);
	selector->setCurrentIndex(index);
	entry = selector->currentText();
	selector->blockSignals(false);
}

// pop up selector
void P2PKaraokeSystem::popupSelector() {
	if (database == NULL)
		return;

	QPushButton *action = qobject_cast<QPushButton *>(sender());
	QString windowTitle;
	if (action == ui->deleteFile)
		windowTitle = "Delete Entry";
	else if (action == ui->editButton)
		windowTitle = "Edit Entry";
	else return;

	QWidget *dialog = new QWidget;
	dialog->setWindowTitle(windowTitle);
	QHBoxLayout *container = new QHBoxLayout;
	dialog->setLayout(container);

	QComboBox *selector = new QComboBox;
	for (int i = 0; i < ui->tableWidget->rowCount(); i++) {
		QString title = ui->tableWidget->item(i, 1)->text().trimmed();
		if (title == "N/A") title = "";
		QString singer = ui->tableWidget->item(i, 2)->text().trimmed();
		if (singer == "N/A") singer = "";
		QString album = ui->tableWidget->item(i, 3)->text().trimmed();
		if (album == "N/A") album = "";
		selector->insertItem(i, "'" + title + "', '" + singer + "', '" + album + "'");
	}
	container->layout()->addWidget(selector);

	QPushButton *ok = new QPushButton("OK");
	container->layout()->addWidget(ok);
	
	entry = selector->currentText();
	if (action == ui->deleteFile)
		connect(ok, SIGNAL(clicked()), this, SLOT(deleteSong()));
	else if (action == ui->editButton)
		connect(ok, SIGNAL(clicked()), this, SLOT(editSong()));
	connect(selector, SIGNAL(currentIndexChanged(int)), this, SLOT(updateSelection(int)));
	connect(ok, SIGNAL(clicked()), dialog, SLOT(close()));

	dialog->show();
}

// clear database
void P2PKaraokeSystem::clearDatabase() {
	for (int i = ui->tableWidget->rowCount(); i > 0; i--)
		ui->tableWidget->removeRow(i - 1);
}

// add new record
void P2PKaraokeSystem::addSong() {
	bool empty;
	bool exist = true;

	// create a database if no database is imported
	if (database == NULL) {
		QString filename = QInputDialog::getText(this, tr("Create database"),
			tr("Database"), QLineEdit::Normal,
			"", &empty);
		if (!empty) return;

		database = QFileDialog::getSaveFileName(
			this,
			tr("Save File"),
			filename,
			"Text File (*.txt)"
		);
		if (database == NULL)
			return;
		exist = false;
	}

	// request for wave file
	QString wav = QFileDialog::getOpenFileName(
		this,
		tr("Open File"),
		"./",
		"Wave File (*.wav)"
	);

	if (wav == NULL)
		return;

	// get the information of the new entry
	QString title = QInputDialog::getText(this, tr("Create entry"),
		tr("Song"), QLineEdit::Normal,
		"", &empty);
	if (!empty) return;

	QString singer = QInputDialog::getText(this, tr("Create entry"),
		tr("Artist"), QLineEdit::Normal,
		"", &empty);
	if (!empty) return;

	QString album = QInputDialog::getText(this, tr("Create entry"),
		tr("Album"), QLineEdit::Normal,
		"", &empty);
	if (!empty) return;

	// add the new entry to database
	fstream file;
	file.open(database.toStdString(), fstream::app);

	//add single quotes
	string wavStd = addSingleQuote(wav.toStdString());
	string titleStd = addSingleQuote(title.toStdString());
	string singerStd = addSingleQuote(singer.toStdString());
	string albumStd = addSingleQuote(album.toStdString());

	if (exist) file << endl;
	file << wavStd << ", " << titleStd << ", " << singerStd << ", " << albumStd;
	file.close();

	// update table
	clearDatabase();
	displayDatabase(database.toStdString());
}

// edit existing record
void P2PKaraokeSystem::editSong() {
	bool empty;

	// get the information of the original entry
	QStringList originalEntry = entry.split(',', QString::SkipEmptyParts);

	QString originalTitle = originalEntry.at(0).trimmed();
	originalTitle.remove(0, 1);
	originalTitle.remove(originalTitle.length() - 1, 1);

	QString originalSinger = originalEntry.at(1).trimmed();
	originalSinger.remove(0, 1);
	originalSinger.remove(originalSinger.length() - 1, 1);

	QString originalAlbum = originalEntry.at(2).trimmed();
	originalAlbum.remove(0, 1);
	originalAlbum.remove(originalAlbum.length() - 1, 1);

	// get the information of the new entry
	QString title = QInputDialog::getText(this, tr("Edit entry"),
		tr("Song"), QLineEdit::Normal,
		originalTitle, &empty);
	if (!empty) return;

	QString singer = QInputDialog::getText(this, tr("Edit entry"),
		tr("Artist"), QLineEdit::Normal,
		originalSinger, &empty);
	if (!empty) return;

	QString album = QInputDialog::getText(this, tr("Edit entry"),
		tr("Album"), QLineEdit::Normal,
		originalAlbum, &empty);
	if (!empty) return;

	//add single quotes
	string titleStd = addSingleQuote(title.toStdString());
	string singerStd = addSingleQuote(singer.toStdString());
	string albumStd = addSingleQuote(album.toStdString());

	// filestream variable files
	ifstream file;
	file.open(database.toStdString());
	ofstream temp;
	temp.open("temp.txt");

	// copy/update entry in temp
	string line;
	for (int i = 0; i < ui->tableWidget->rowCount(); i++) {
		getline(file, line);
		if (i != 0) temp << endl;
		if (line.find(entry.toStdString()) == string::npos)
			temp << line;
		else {
			istringstream wav(line);
			string wavStd;
			getline(wav, wavStd, ',');
			temp << wavStd << ", " << titleStd << ", " << singerStd << ", " << albumStd;
		}
	}

	// update database
	file.close();
	temp.close();
	remove(database.toStdString().c_str());
	rename("temp.txt", database.toStdString().c_str());

	// update table
	clearDatabase();
	displayDatabase(database.toStdString());
}

// delete existing record
void P2PKaraokeSystem::deleteSong() {
	// filestream variable files
	ifstream file;
	file.open(database.toStdString());
	ofstream temp;
	temp.open("temp.txt");

	// remove entry to be deleted in temp
	string line;
	bool firstEntry = true;
	for (int i = 0; i < ui->tableWidget->rowCount(); i++) {
		getline(file, line);
		if (line.find(entry.toStdString()) == string::npos) {
			if (firstEntry)
				firstEntry = false;
			else temp << endl;
			temp << line;
		}
	}

	// update database
	file.close();
	temp.close();
	remove(database.toStdString().c_str());
	rename("temp.txt", database.toStdString().c_str());

	// update table
	clearDatabase();
	displayDatabase(database.toStdString());
}

// display database
void P2PKaraokeSystem::displayDatabase(string filename) {
	// filestream variable file
	ifstream file;

	// open file
	file.open(filename);

	// extracting words form the file
	string wav, title, singer, album;
	while (!file.eof()) {
		int row = ui->tableWidget->rowCount();
		ui->tableWidget->setRowCount(row + 1);

		getline(file, wav, ',');
		wav = removeSingleQuote(wav);
		if (wav == " ") wav = "N/A";
		ui->tableWidget->setItem(row, 0, new QTableWidgetItem(QString::fromStdString(wav)));
		
		getline(file, title, ',');
		title = removeSingleQuote(title);
		if (title == " ") title = "N/A";
		ui->tableWidget->setItem(row, 1, new QTableWidgetItem(QString::fromStdString(title)));
		
		getline(file, singer, ',');
		singer = removeSingleQuote(singer);
		if (singer == " ") singer = "N/A";
		ui->tableWidget->setItem(row, 2, new QTableWidgetItem(QString::fromStdString(singer)));
		
		getline(file, album, '\n');
		album = removeSingleQuote(album);
		if (album == " ") album = "N/A";
		ui->tableWidget->setItem(row, 3, new QTableWidgetItem(QString::fromStdString(album)));
	}

	// close file
	file.close();
}

// load database -> pop up QfileDialog
void P2PKaraokeSystem::loadDatabase() {
	database = QFileDialog::getOpenFileName(
		this,
		tr("Open File"),
		"./",
		"Text File (*.txt)"
	);

	if (database == NULL)
		return;
	if (playFlag) togglePlay();
	ui->nowPlaying->clear();
	ui->searchField->clear();
	clearDatabase();
	displayDatabase(database.toStdString());
}

// select item -> play song
void P2PKaraokeSystem::playSong(QTableWidgetItem* item) {
	if (playFlag) togglePlay();

	QTableWidgetItem *wav = ui->tableWidget->item(item->row(), 0);
	ui->nowPlaying->setText(wav->text());
	togglePlay();
}

// play/pause
void P2PKaraokeSystem::togglePlay() {
	QString filename = ui->nowPlaying->text();
	if (filename == NULL)
		return;

	QFile file(filename);
	if (!file.exists()) {
		QMessageBox::critical(this, tr("Error"),
			tr("File does not exist"));
		return;
	}

	if (!playFlag) {
		player = new musicPlayer((LPWSTR)filename.utf16());
		player->write();
		player->play();

		playFlag = true;
		timer->setInterval(player->waveIn->nBlocks * 10);
		timer->start();

		QIcon icon;
		icon.addFile(QStringLiteral("stop.png"), QSize(), QIcon::Normal, QIcon::Off);
		ui->playButton->setIcon(icon);

		loadLyric();
	}
	else {
		player->stop();
		player->~musicPlayer();

		playFlag = false;
		timer->stop();

		QIcon icon;
		icon.addFile(QStringLiteral("play.png"), QSize(), QIcon::Normal, QIcon::Off);
		ui->playButton->setIcon(icon);

		ui->progressBar->setValue(0);
		ui->lyricBox->clear();
		if (!lyricTimer.isEmpty()) {
			qDeleteAll(lyricTimer);
			lyricTimer.clear();
		}
	}
}

// previous song
void P2PKaraokeSystem::playPrev() {
	QString current = ui->nowPlaying->text();
	if (current == NULL)
		return;

	QList<QTableWidgetItem*> list = ui->tableWidget->findItems(current, Qt::MatchExactly);
	int row = list.first()->row() - 1;
	if (ui->shuffle->isChecked() && ui->tableWidget->rowCount() != 0)
		row = rand() % ui->tableWidget->rowCount();
	if (row < 0)
		return;

	QTableWidgetItem *wav = ui->tableWidget->item(row, 0);
	playSong(wav);
}

// next song
void P2PKaraokeSystem::playNext() {
	QString current = ui->nowPlaying->text();
	if (current == NULL)
		return;

	QList<QTableWidgetItem*> list = ui->tableWidget->findItems(current, Qt::MatchExactly);
	int row = list.first()->row() + 1;
	if (ui->shuffle->isChecked() && ui->tableWidget->rowCount() != 0)
		row = rand() % ui->tableWidget->rowCount();
	if (row >= ui->tableWidget->rowCount())
		return;

	QTableWidgetItem *wav = ui->tableWidget->item(row, 0);
	playSong(wav);
}

// update process bar
void P2PKaraokeSystem::updateProgress() {
	int progress = ui->progressBar->value();
	if (progress == 100)
		return;

	progress += 1;
	ui->progressBar->setValue(progress);

	if (progress == 100) {
		togglePlay();
		if (ui->autoplay->isChecked())
			playNext();
	}
}

// search song
void P2PKaraokeSystem::searchSong() {
	QString text = ui->searchField->toPlainText();
	QList<QTableWidgetItem*> list = ui->tableWidget->findItems(text, Qt::MatchContains);
	int count = 0;
	for (int i = 0; i < ui->tableWidget->rowCount(); i++) {
		if (!list.isEmpty() && i == list.at(count)->row()) {
			ui->tableWidget->setRowHidden(i, false);
			if (count != list.size() - 1)
				count++;
		}
		else ui->tableWidget->setRowHidden(i, true);
	}
}

// load lrc
void P2PKaraokeSystem::loadLyric() {
	QString path = ui->nowPlaying->text();
	if (path == NULL)
		return;

	path.chop(4);
	QString filename = path.append(".lrc");
	QFile lyricFile(filename);
	if (!lyricFile.open(QIODevice::ReadOnly)) {
		ui->lyricBox->setText("No lyric is available");
		return;
	}
	QTextStream input(&lyricFile);
	QString qs = input.readLine();
	while (!qs.isEmpty()) {
		setLyric(qs);
		qs = input.readLine();
	}
}

// set lyric
void P2PKaraokeSystem::setLyric(QString lyric) {
	QRegExp lex("[\\[\\]]");
	QRegExp timex("[:.]");
	QStringList List = lyric.split(lex, QString::SkipEmptyParts);
	QString Time = List.at(0);
	if (List.length() > 1) {
		QStringList TList = Time.split(timex);
		QString MM = TList.at(0);
		QString SS = TList.at(1);
		int length = MM.toInt() * 60 * 1000;
		length += SS.toInt() * 1000;

		QTimer *ltimer = new QTimer(this);
		ltimer->setSingleShot(true);
		ltimer->setInterval(length);

		QRegExp regex("[\\[\\d\\d:\\d\\d.\\d\\d\\]]");
		lyric.remove(regex).toStdString();

		QSignalMapper *signalMapper = new QSignalMapper(this);
		connect(ltimer, SIGNAL(timeout()), signalMapper, SLOT(map()));
		signalMapper->setMapping(ltimer, lyric);
		connect(signalMapper, SIGNAL(mapped(QString)), this, SLOT(displayLyric(QString)));
		ltimer->start();
		lyricTimer.append(ltimer);
	}
}

// display lyric
void P2PKaraokeSystem::displayLyric(QString lyric) {
	ui->lyricBox->setText(lyric);
	QTimer *temp = lyricTimer[0];
	temp->~QTimer();
	lyricTimer.pop_front();
}

// toggle p2p connection
void P2PKaraokeSystem::toggleConnect() {
	if (connectFlag) {
		int isOK = QMessageBox::information(NULL, "P2P Connection",
			"Are you sure to disconnect?",
			QMessageBox::Yes | QMessageBox::No,
			QMessageBox::Yes);
		if (isOK == QMessageBox::Yes) {
			p2pClient->~client();
			p2pServer->~server();
			connectFlag = false;
			ui->connectButton->setText("Start P2P Connection");
		}
	}
	else {
		QInputDialog getIP = new QInputDialog;
		getIP.setWindowTitle("P2P Connection");
		getIP.setLabelText("Please input central server IP Address");
		getIP.setInputMode(QInputDialog::TextInput);
		getIP.resize(500, 200);
		int x = this->x() + (this->width() - getIP.width()) / 2;
		int y = this->y() + (this->height() - getIP.height()) / 2;
		getIP.move(x, y);

		int isOK = getIP.exec();
		if (isOK) {
			QString message;

			// set up as server
			p2pServer = new server();
			if (p2pServer->err > 0) {
				message = "Failed";
				QMessageBox::information(NULL, "P2P Set Up",
					"Server Set Up " + message);
				return;
			}

			// connect to central servers
			QString ipAddr = getIP.textValue();
			p2pClient = new client(ipAddr.toLocal8Bit().data());
			if (p2pClient->err > 0) message = "Failed";
			else {
				message = "Succeeded";
				connectFlag = true;
				ui->connectButton->setText("Stop P2P Connection");
			}

			QMessageBox::information(NULL, "IP Address " + ipAddr,
				"Connection " + message);
			if (message == "Failed")
				return;

			// create threads that always connect to central server
			serverThread = std::thread(&server::serverWorking, p2pServer);
			clientThread = std::thread(&client::receiveData, p2pClient);

			clientThread.join();
			while (!p2pClient->mtx.try_lock());
			message.clear();

			// parse peer list
			char* peerArr;
			peerArr = strtok(p2pClient->message, "\n");
			while (peerArr != NULL) {
				message += QString::fromStdString(std::string(peerArr) + "\n");
				p2pServer->createPeer(-1, peerArr);
				peerArr = strtok(NULL, "\n");
			}
			p2pClient->mtx.unlock();

			// pop up current peer network
			QMessageBox::information(NULL, "Connected Peers", message);
		}
	}
}