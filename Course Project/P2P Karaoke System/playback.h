#pragma once

#ifndef _PLAYBACK_H_
#define _PLAYBACK_H_

#pragma comment(lib, "winmm.lib")
#include <windows.h>
#include <mmsystem.h>
#include <stdio.h>
#include <math.h>

class audioPlayer{
public:
	audioPlayer(LPWAVEFORMATEX waveFormat);
	~audioPlayer();

	HWAVEOUT wavePlayer;
	LPWAVEHDR waveData;
	int openError;

	MMRESULT setVolume(DWORD volume) {
		return waveOutSetVolume(wavePlayer, volume);
	}

	void initializeHeader(int nBytes) {
		waveData = (LPWAVEHDR)malloc(sizeof(LPWAVEHDR));
		waveData->lpData = (LPSTR)malloc(sizeof(BYTE) * nBytes);
		waveData->dwBufferLength = nBytes;
		waveData->dwUser = 0;
		waveData->dwFlags = WHDR_PREPARED;
	}

	void writeHeader(LPSTR data, DWORD bufferLength) {
		memcpy(waveData->lpData + waveData->dwUser, data, bufferLength);
		waveData->dwUser += bufferLength;
	}

	MMRESULT prepareHeader() {
		return waveOutPrepareHeader(wavePlayer, waveData, sizeof(WAVEHDR));
	}

	MMRESULT unprepareHeader() {
		return waveOutUnprepareHeader(wavePlayer, waveData, sizeof(WAVEHDR));
	}
};

class audioFile{
public:
	audioFile(LPWSTR path);
	~audioFile();

	HMMIO waveFile;
	WAVEFORMATEX waveFormat;
	int nSamples;
	int nSamplesRead = 0;
	int nBlocks;
	int nBlocksRead = 0;
	int blockSize;

	int readChunkInfo() {
		// go into RIFF chunk
		MMCKINFO riffChunk;
		riffChunk.fccType = mmioFOURCC('W', 'A', 'V', 'E');
		if (mmioDescend(waveFile, &riffChunk, NULL, MMIO_FINDRIFF) != MMSYSERR_NOERROR) {
			printf("Error: Unable to go into RIFF chunk\n");
			return -1;
		}

		// go into FMT chunk
		MMCKINFO fmtChunk;
		fmtChunk.ckid = mmioFOURCC('f', 'm', 't', ' ');
		if (mmioDescend(waveFile, &fmtChunk, &riffChunk, MMIO_FINDCHUNK) != MMSYSERR_NOERROR) {
			printf("Error: Unable to go into fmt chunk\n");
			return -1;
		}
		if (mmioRead(waveFile, (char*)&waveFormat, fmtChunk.cksize) != fmtChunk.cksize) {
			printf("Error: Unable to read fmt chunk\n");
			return -1;
		}
		if (mmioAscend(waveFile, &fmtChunk, 0) != MMSYSERR_NOERROR) {
			printf("Error: Unable to escape from fmt chunk");
			return -1;
		}

		// go into DATA chunk
		MMCKINFO dataChunk;
		dataChunk.ckid = mmioFOURCC('d', 'a', 't', 'a');
		if (mmioDescend(waveFile, &dataChunk, &riffChunk, MMIO_FINDCHUNK) != MMSYSERR_NOERROR) {
			printf("Error: Unable to go into data chunk\n");
			return -1;
		}

		// declare the number of samples and blocks in total
		nSamples = dataChunk.cksize / waveFormat.nBlockAlign;
		nBlocks = ceil((double)nSamples / (double)waveFormat.nSamplesPerSec);
		blockSize = waveFormat.nSamplesPerSec * waveFormat.nBlockAlign;
		return 0;
	}

	int readBuffer(WAVEHDR *buffer) {
		// return if no block is available
		if (nBlocksRead > nBlocks) {
			printf("Error: # blocks read exceeds # blocks available\n");
			return -1;
		}
		else if (nBlocksRead == nBlocks)
			return 0;

		// declare the number of samples to be read
		int nSamplesToRead = 0;
		if (nSamplesRead + waveFormat.nSamplesPerSec <= nSamples)
			nSamplesToRead = waveFormat.nSamplesPerSec;
		else nSamplesToRead = nSamples - nSamplesRead;

		// read the data into buffer
		int nBytesToRead = nSamplesToRead * waveFormat.nBlockAlign;
		if (mmioRead(waveFile, (char*)buffer->lpData, nBytesToRead) != nBytesToRead) {
			printf("Error: Unable to play back\n");
			return -1;
		}

		// update pointer values
		buffer->dwBufferLength = nBytesToRead;
		nSamplesRead += nSamplesToRead;
		nBlocksRead += 1;
		return 0;
	}
};

class musicPlayer{
public:
	musicPlayer(LPWSTR filename);
	~musicPlayer();

	audioFile *waveIn;
	audioPlayer *waveOut;

	void write();
	void play();
	void stop();
};

WAVEHDR* createBuffer(int);

#endif // !_PLAYBACK_H_