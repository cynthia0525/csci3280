/****************************************************************************
** Meta object code from reading C++ file 'P2PKaraokeSystem.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.7.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../P2PKaraokeSystem.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'P2PKaraokeSystem.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.7.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_P2PKaraokeSystem_t {
    QByteArrayData data[17];
    char stringdata0[198];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_P2PKaraokeSystem_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_P2PKaraokeSystem_t qt_meta_stringdata_P2PKaraokeSystem = {
    {
QT_MOC_LITERAL(0, 0, 16), // "P2PKaraokeSystem"
QT_MOC_LITERAL(1, 17, 12), // "loadDatabase"
QT_MOC_LITERAL(2, 30, 0), // ""
QT_MOC_LITERAL(3, 31, 7), // "addSong"
QT_MOC_LITERAL(4, 39, 8), // "editSong"
QT_MOC_LITERAL(5, 48, 10), // "deleteSong"
QT_MOC_LITERAL(6, 59, 13), // "popupSelector"
QT_MOC_LITERAL(7, 73, 15), // "updateSelection"
QT_MOC_LITERAL(8, 89, 8), // "playSong"
QT_MOC_LITERAL(9, 98, 17), // "QTableWidgetItem*"
QT_MOC_LITERAL(10, 116, 10), // "togglePlay"
QT_MOC_LITERAL(11, 127, 8), // "playPrev"
QT_MOC_LITERAL(12, 136, 8), // "playNext"
QT_MOC_LITERAL(13, 145, 14), // "updateProgress"
QT_MOC_LITERAL(14, 160, 10), // "searchSong"
QT_MOC_LITERAL(15, 171, 12), // "displayLyric"
QT_MOC_LITERAL(16, 184, 13) // "toggleConnect"

    },
    "P2PKaraokeSystem\0loadDatabase\0\0addSong\0"
    "editSong\0deleteSong\0popupSelector\0"
    "updateSelection\0playSong\0QTableWidgetItem*\0"
    "togglePlay\0playPrev\0playNext\0"
    "updateProgress\0searchSong\0displayLyric\0"
    "toggleConnect"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_P2PKaraokeSystem[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      14,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   84,    2, 0x08 /* Private */,
       3,    0,   85,    2, 0x08 /* Private */,
       4,    0,   86,    2, 0x08 /* Private */,
       5,    0,   87,    2, 0x08 /* Private */,
       6,    0,   88,    2, 0x08 /* Private */,
       7,    1,   89,    2, 0x08 /* Private */,
       8,    1,   92,    2, 0x08 /* Private */,
      10,    0,   95,    2, 0x08 /* Private */,
      11,    0,   96,    2, 0x08 /* Private */,
      12,    0,   97,    2, 0x08 /* Private */,
      13,    0,   98,    2, 0x08 /* Private */,
      14,    0,   99,    2, 0x08 /* Private */,
      15,    1,  100,    2, 0x08 /* Private */,
      16,    0,  103,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,    2,
    QMetaType::Void, 0x80000000 | 9,    2,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,    2,
    QMetaType::Void,

       0        // eod
};

void P2PKaraokeSystem::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        P2PKaraokeSystem *_t = static_cast<P2PKaraokeSystem *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->loadDatabase(); break;
        case 1: _t->addSong(); break;
        case 2: _t->editSong(); break;
        case 3: _t->deleteSong(); break;
        case 4: _t->popupSelector(); break;
        case 5: _t->updateSelection((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 6: _t->playSong((*reinterpret_cast< QTableWidgetItem*(*)>(_a[1]))); break;
        case 7: _t->togglePlay(); break;
        case 8: _t->playPrev(); break;
        case 9: _t->playNext(); break;
        case 10: _t->updateProgress(); break;
        case 11: _t->searchSong(); break;
        case 12: _t->displayLyric((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 13: _t->toggleConnect(); break;
        default: ;
        }
    }
}

const QMetaObject P2PKaraokeSystem::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_P2PKaraokeSystem.data,
      qt_meta_data_P2PKaraokeSystem,  qt_static_metacall, Q_NULLPTR, Q_NULLPTR}
};


const QMetaObject *P2PKaraokeSystem::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *P2PKaraokeSystem::qt_metacast(const char *_clname)
{
    if (!_clname) return Q_NULLPTR;
    if (!strcmp(_clname, qt_meta_stringdata_P2PKaraokeSystem.stringdata0))
        return static_cast<void*>(const_cast< P2PKaraokeSystem*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int P2PKaraokeSystem::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 14)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 14;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 14)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 14;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
