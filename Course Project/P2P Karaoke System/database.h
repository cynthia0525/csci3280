#pragma once

#ifndef _DATABASE_H_
#define _DATABASE_H_

using namespace std;

#include <fstream>
#include <string.h>
#include <algorithm>

// remove single quotes
string removeSingleQuote(string str) {
	string chars = "\'";
	for (char c : chars)
		str.erase(remove(str.begin(), str.end(), c), str.end());
	return str;
}

// add single quotes
string addSingleQuote(string str) {
	string singlequote = "\'";
	str.insert(0, singlequote);
	str += singlequote;
	return str;
}

#endif // !_DATABASE_H_
