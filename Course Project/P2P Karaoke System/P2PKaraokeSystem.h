#pragma once

#include "client.h"
#include "server.h"
#include "database.h"
#include "playback.h"
#include <QtWidgets/QMainWindow>
#include <QTimer>
#include "ui_P2PKaraokeSystem.h"

class P2PKaraokeSystem : public QMainWindow
{
	Q_OBJECT

public:
	P2PKaraokeSystem(QWidget *parent = Q_NULLPTR);

private slots:
	void loadDatabase();
	void addSong();
	void editSong();
	void deleteSong();
	void popupSelector();
	void updateSelection(int);
	void playSong(QTableWidgetItem*);
	void togglePlay();
	void playPrev();
	void playNext();
	void updateProgress();
	void searchSong();
	void displayLyric(QString);
	void toggleConnect();

private:
	boolean playFlag = false;
	boolean connectFlag = false;

	Ui::MainWindow *ui;
	musicPlayer *player;
	QTimer *timer = new QTimer(this);

	void displayDatabase(string);
	void clearDatabase();
	void loadLyric();
	void setLyric(QString);
};
