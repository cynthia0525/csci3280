#include "playback.h"
#include <stdio.h>
#include <synchapi.h>

audioPlayer::audioPlayer(LPWAVEFORMATEX waveFormat) {
	if (waveFormat == NULL)
		return;
	openError = waveOutOpen(&wavePlayer, WAVE_MAPPER, waveFormat, NULL, 0, CALLBACK_NULL);
}

audioPlayer::~audioPlayer() {
	waveOutClose(wavePlayer);
}


audioFile::audioFile(LPWSTR path) {
	if (path == NULL)
		return;
	nSamples = nBlocks = blockSize = 0;
	waveFile = mmioOpen(path, NULL, MMIO_READ);
}

audioFile::~audioFile() {
	mmioClose(waveFile, 0);
}

WAVEHDR* createBuffer(int nBytes) {
	WAVEHDR *bufferBlock = (WAVEHDR*)malloc(sizeof(WAVEHDR));
	bufferBlock->lpData = (LPSTR)malloc(sizeof(BYTE) * nBytes);
	bufferBlock->dwBufferLength = 0;
	bufferBlock->dwFlags = WHDR_PREPARED;
	bufferBlock->lpNext = NULL;
	return bufferBlock;
}


musicPlayer::musicPlayer(LPWSTR filename) {
	if (filename == NULL)
		return;

	// open wave file
	waveIn = new audioFile(filename);
	if (waveIn->waveFile == NULL) {
		printf("Error: Unable to open wav file\n");
		return;
	}

	// read wave specification
	if (waveIn->readChunkInfo() == -1)
		return;

	// open wave player
	waveOut = new audioPlayer(&waveIn->waveFormat);
	if (waveOut->openError != MMSYSERR_NOERROR) {
		printf("Error: Unable to open audio player\n");
		return;
	}

	// set output volume
	if (waveOut->setVolume(0xFFFF) != MMSYSERR_NOERROR) {
		printf("Error: Unable to set output volume\n");
		return;
	}

	// prepare buffer
	waveOut->initializeHeader(waveIn->nSamples * waveIn->waveFormat.nBlockAlign);

	// prepare header
	if (waveOut->prepareHeader() != MMSYSERR_NOERROR) {
		printf("Error: Unable to prepare buffer\n");
		return;
	}
}

musicPlayer::~musicPlayer() {
	// unprepare header
	if (waveOut->unprepareHeader() != MMSYSERR_NOERROR) {
		printf("Error: Unable to unprepare buffer\n");
		return;
	}

	// close wave file
	waveIn->~audioFile();

	// close wave player
	waveOut->~audioPlayer();
}

void musicPlayer::write() {
	while (waveIn->nBlocksRead < waveIn->nBlocks) {
		WAVEHDR* temp = createBuffer(waveIn->blockSize);
		if (waveIn->readBuffer(temp) == -1)
			return;
		waveOut->writeHeader(temp->lpData, temp->dwBufferLength);
		free(temp);
	}
}

void musicPlayer::play() {
	if (waveOutWrite(waveOut->wavePlayer, waveOut->waveData, sizeof(WAVEHDR)) != MMSYSERR_NOERROR) {
		printf("Error: Unable to start playing\n");
		return;
	}
}

void musicPlayer::stop() {
	if (waveOutReset(waveOut->wavePlayer) != MMSYSERR_NOERROR) {
		printf("Error: Unable to stop playing\n");
		return;
	}
}
