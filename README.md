# CSCI3280 Introduction to Multimedia Systems (Spring 2019)
This course covers the design and implementation of modern multimedia systems. Topics include multimedia systems design, multimedia data representation, multimedia hardware and software, multimedia communication and networking, multimedia programming and multimedia information systems.

# Grading
* 15% Assignment 1 [100/100]
* 15% Assignment 2 [100/100]
* 20% Course Project [60/100]
* 50% Final Exam

# Remarks
Please follow the instructions mentioned in the specification to compile and execute the programs.