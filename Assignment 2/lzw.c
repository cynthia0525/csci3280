/*
* CSCI3280 Introduction to Multimedia Systems *
* --- Declaration --- *
* I declare that the assignment here submitted is original except for source
* material explicitly acknowledged. I also acknowledge that I am aware of
* University policy and regulations on honesty in academic work, and of the
* disciplinary guidelines and procedures applicable to breaches of such policy
* and regulations, as contained in the website
* http://www.cuhk.edu.hk/policy/academichonesty/ *
* Assignment 2
* Name : CHAN Ka Yi
* Student ID : 1155093641
* Email Addr : kychan7@cse.cuhk.edu.hk
*/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <ctype.h>

#define CODE_SIZE 12
#define ENTRY_SIZE 4096
#define TRUE 1
#define FALSE 0

/* data structure */
unsigned char dict[ENTRY_SIZE][ENTRY_SIZE];
int nCode, nHash;
void initdict();
void addword(unsigned char*);
int search(int, unsigned char*);
unsigned int hashing(int, unsigned char*);

/* function prototypes */
unsigned int read_code(FILE *, unsigned int);
void write_code(FILE*, unsigned int, unsigned int);
void writefileheader(FILE *, char**, int);
void readfileheader(FILE *, char**, int *);
void compress(FILE*, FILE*);
void decompress(FILE*, FILE*);


int main(int argc, char **argv)
{
    int printusage = 0;
    int	no_of_file;
    char **input_file_names;
	char *output_file_names;
    FILE *lzw_file;

    if (argc >= 3)
    {
		if (strcmp(argv[1],"-c") == 0)
		{
			/* compression */
			lzw_file = fopen(argv[2] ,"wb");

			/* write the file header */
			input_file_names = argv + 3;
			no_of_file = argc - 3;
			writefileheader(lzw_file,input_file_names,no_of_file);

			initdict();
			for (int i = 0; i < no_of_file; i++) {
				FILE *input_file = fopen(input_file_names[i], "rb");
				printf("Compress: %s ", input_file_names[i]);
				compress(input_file, lzw_file);
				fclose(input_file);
			}
        	write_code(lzw_file, 0, CODE_SIZE);

			fclose(lzw_file);
		} else
		if (strcmp(argv[1],"-d") == 0)
		{
			/* decompress */
			lzw_file = fopen(argv[2] ,"rb");
			
			/* read the file header */
			no_of_file = 0;
			readfileheader(lzw_file,&output_file_names,&no_of_file);
			
			char *output_file_name = strtok(output_file_names, "\n");
			initdict();
			for (int i = 0; i < no_of_file; i++) {
				FILE *output_file = fopen(output_file_name, "wb");
				printf("Decompress: %s ", output_file_name);
				decompress(lzw_file, output_file);
				fclose(output_file);
				output_file_name = strtok(NULL, "\n");
			}
			
			fclose(lzw_file);
			free(output_file_names);
		} else
			printusage = 1;
    } else
		printusage = 1;
	
	if (printusage)
		printf("Usage: %s -<c/d> <lzw filename> <LinkedList of files>\n",argv[0]);
 	
	return 0;
}


/*****************************************************************
 *
 * writefileheader() -  write the lzw file header to support multiple files
 *
 ****************************************************************/
void writefileheader(FILE *lzw_file,char** input_file_names,int no_of_files)
{
	int i;
	/* write the file header */
	for (i = 0 ; i < no_of_files; i++) 
	{
		fprintf(lzw_file,"%s\n",input_file_names[i]);
	}
	fputc('\n',lzw_file);
}

/*****************************************************************
 *
 * readfileheader() - read the fileheader from the lzw file
 *
 ****************************************************************/
void readfileheader(FILE *lzw_file,char** output_filenames,int * no_of_files)
{
	int noofchar;
	char c,lastc;

	noofchar = 0;
	lastc = 0;
	*no_of_files=0;
	/* find where is the end of double newline */
	while((c = fgetc(lzw_file)) != EOF)
	{
		noofchar++;
		if (c == '\n')
		{
			if (lastc == c)
				/* found double newline */
				break;
			(*no_of_files)++;
		}
		lastc = c;
	}

	if (c == EOF)
	{
		/* problem .... file may have corrupted */
		*no_of_files = 0;
		return;
	}
	/* allocate memeory for the filenames */
	*output_filenames = (char *) malloc(sizeof(char)*noofchar);
	/* roll back to start */
	fseek(lzw_file,0,SEEK_SET);

	fread((*output_filenames),1,(size_t)noofchar,lzw_file);
	
	return;
}


/*****************************************************************
 *
 * read_code() - reads a specific-size code from the code file
 *
 ****************************************************************/
unsigned int read_code(FILE *input, unsigned int code_size)
{
    unsigned int return_value;
    static int input_bit_count = 0;
    static unsigned long input_bit_buffer = 0L;

    /* The code file is treated as an input bit-stream. Each     */
    /*   character read is stored in input_bit_buffer, which     */
    /*   is 32-bit wide.                                         */

    /* input_bit_count stores the no. of bits left in the buffer */

    while (input_bit_count <= 24) {
        input_bit_buffer |= (unsigned long) getc(input) << (24-input_bit_count);
        input_bit_count += 8;
    }
    
    return_value = input_bit_buffer >> (32 - code_size);
    input_bit_buffer <<= code_size;
    input_bit_count -= code_size;
    
    return(return_value);
}

/*****************************************************************
 *
 * write_code() - write a code (of specific length) to the file 
 *
 ****************************************************************/
void write_code(FILE *output, unsigned int code, unsigned int code_size)
{
    static int output_bit_count = 0;
    static unsigned long output_bit_buffer = 0L;

    /* Each output code is first stored in output_bit_buffer,    */
    /*   which is 32-bit wide. Content in output_bit_buffer is   */
    /*   written to the output file in bytes.                    */

    /* output_bit_count stores the no. of bits left              */

    output_bit_buffer |= (unsigned long) code << (32-code_size-output_bit_count);
    output_bit_count += code_size;

    while (output_bit_count >= 8) {
        putc(output_bit_buffer >> 24, output);
        output_bit_buffer <<= 8;
        output_bit_count -= 8;
    }

    /* only < 8 bits left in the buffer                          */
}


/*****************************************************************
 *
 * compress() - compress the source file and output the coded text
 *
 ****************************************************************/
void compress(FILE *input, FILE *output)
{
	const int compressedSize = ftell(output);
	unsigned int code = EOF;
	unsigned char p[ENTRY_SIZE] = "";
	int c;
	
	while ((c = fgetc(input)) != EOF) {
		// str := p + c
		unsigned char str[ENTRY_SIZE];
		strcpy(str, p);
		unsigned char s[2] = { c, '\0' };
		strcat(str, s);

		unsigned int index;
		// search for str in dict
		int res = FALSE;
		for (int i = 0; i < nHash; i++) {
			index = hashing(i, str);
			if ((res = search(index, str)) == TRUE || search(index, "") == TRUE)
				break;
		}

		// search for the longest pattern
		if (res == TRUE) {
			code = index;
			unsigned char s[2] = { c, '\0' };
			strcat(p, s);
		}
		
		// add and output the codeword
		else {
			write_code(output, code, CODE_SIZE);
			addword(str);
			// p := c
			unsigned char s[2] = { c, '\0' };
			strcpy(p, s);
			code = hashing(0, p);
		}
	}
	write_code(output, code, CODE_SIZE);
	printf("[%d byte -> %d byte]\n", ftell(input), ftell(output) - compressedSize);

	// write the separation indicator
	write_code(output, ENTRY_SIZE - 1, CODE_SIZE);
}

/*****************************************************************
 *
 * decompress() - decompress a compressed file to the orig. file
 *
 ****************************************************************/
void decompress(FILE *input, FILE *output)
{
	const int decompressedSize = ftell(input);
	unsigned int code = ENTRY_SIZE - 1;
	unsigned char p[ENTRY_SIZE] = "";
	int c;

	while ((code = read_code(input, CODE_SIZE)) != ENTRY_SIZE - 1) {
		// output the codeword
		unsigned char str[ENTRY_SIZE];
		strcpy(str, dict[code]);
			// exception handling
			if (search(code, "") == TRUE) {
				strcpy(str, p);
				unsigned char s[2] = { p[0], '\0' };
				strcat(str, s);
			}
		fputs(str, output);

		// str := p + c
		c = str[0];
		unsigned char s[2] = { c, '\0' };
		strcat(p, s);

		unsigned int index;
		// search for str in dict
		int res = FALSE;
		for (int i = 0; i < nHash; i++) {
			index = hashing(i, p);
			if ((res = search(index, p)) == TRUE || search(index, "") == TRUE)
				break;
		}

		// add the codeword
		if (res != TRUE) addword(p);
		
		// p := str
		strcpy(p, str);
	}
	printf("[%d byte -> %d byte]\n", ftell(input) - decompressedSize, ftell(output));
}

// initialize the dictionary
void initdict() {
	for (int i = 0; i < ENTRY_SIZE; i++) {
		if (i < 256) {
			unsigned char str[2] = { i, '\0' };
			strcpy(dict[i], str);
		}
		else strcpy(dict[i], "");
	}
	nCode = 256;
	nHash = 1;
}

// add a new word to the dictionary
void addword(unsigned char* code) {
	if (nCode == ENTRY_SIZE - 1)
		initdict();
	
	unsigned int index;
	int i = 0;
	while (TRUE) {
		index = hashing(i, code);
		// exit the loop when an empty entry is found
		if (index != 0 && search(index, "") == TRUE)
			break;
		i++;
	}
	if (i > nHash) nHash = i;
	strcpy(dict[index], code);

	nCode++;
}

// search for the codeword
int search(int index, unsigned char* code) {
	if (strcmp(dict[index], code) == 0)
		return TRUE;
	else return FALSE;
}

// hash function
unsigned int hashing(int k, unsigned char* code) {
	unsigned int temp = 0;
	for (int i = 0; i < strlen(code); i++) {
		temp += (i * 88 + 1) * code[i];
	}
	temp += k * k * k * k * k;
	return temp % (ENTRY_SIZE - 1);
}
