	CSCI3280 Assignment 2 LZW Compression

Name: CHAN Ka Yi
Student ID: 1155093641

Points to note: 
	Hash table (closed hashing) of 4096 entries is used in compression.
	The basic hash function is the sum of (i * 88 + 1) * code[i], where i is the ith character of the string to be encoded.
	To reduce collision problem, quintic probing (k^5) is added to the total mentioned above.
	At last, hashing(str) + probing(k) mod 4095 will be the value of returned index.
	The number of collision of each newly built dictionary is now reduced from 100,000 to 30,000.