/*

CSCI 3280, Introduction to Multimedia Systems
Spring 2019

Assignment 01 Skeleton

ascii.cpp

*/

#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "malloc.h"
#include "memory.h"
#include "math.h"
#include "bmp.h"		//	Simple .bmp library

#define MAX_SHADES 8

char shades[MAX_SHADES] = {'@','#','%','*','|','-','.',' '};

#define SAFE_FREE(p)  { if(p){ free(p);  (p)=NULL;} }


int main(int argc, char** argv)
{
	if (argc < 2) {
		printf("Usage: ascii_bonus <s|p> <input.bmp> <width> <height> <output.txt> <output.html>\n");
		return 0;
	}

	//
	//	1. Open BMP file
	//
	Bitmap image_data(argv[2]);

	if(image_data.getData() == NULL) {
		printf("unable to load bmp image!\n");
		return -1;
	}

	int width = image_data.getWidth();
	int height = image_data.getHeight();

	//
	//	2. Obtain Luminance
	//
	Bitmap bw_image(width, height);
	for (int i = 0; i < width; i++) {
		for (int j = 0; j < height; j++) {
			unsigned char r, g, b, y;
			image_data.getColor(i, j, r, g, b);
			y = r * 0.299 + g * 0.587 + b * 0.114;
			bw_image.setColor(i, j, y, y, y);
		}
	}
	bw_image.save("b&w.bmp");

	//
	//  3. Resize image
	//
	if (argc > 2) {
		int resample_width = atoi(argv[3]);
		int resample_height = atoi(argv[4]);

		// resize w/o deformation
		if (resample_width == 0 && resample_height == 0) {
			resample_width = width;
			resample_height = height;
		}
		else if (resample_width == 0 && resample_height != 0)
			resample_width = resample_height;
		else if (resample_width != 0 && resample_height == 0)
			resample_height = resample_width;
		
		// declare the resize ratio of each side
		int count_w = 1;
		while (width/count_w > resample_width)
			count_w++;
		int count_h = 1;
		while (height/count_h > resample_height)
			count_h++;
        // keep the same ratio if deformation is not applicable
		if (atoi(argv[3]) == 0 || atoi(argv[4]) == 0) {
			if (count_w < count_h)
				count_w = count_h;
			else count_h = count_w;
		}
        // redefine the width and height of the resized image
		resample_width = width/count_w;
		resample_height = height/count_h;

		Bitmap resample_image(resample_width, resample_height);
		for (int i = 0; i < width; i++) {
			int temp = 0;
			for (int j = 0; j < height; j++) {
				unsigned char y;
				bw_image.getColor(i, j, y, y, y);
				temp += y;
				// resample height
				if ((j+1)%count_h == 0) {
					unsigned char resized_y = temp/count_h;
					bw_image.setColor(i, j/count_h, resized_y, resized_y, resized_y);
					temp = 0;
					// resample width
					if ((i+1)%count_w == 0) {
						for (int k = 0; k < count_w; k++) {
							bw_image.getColor(i-k, j/count_h, y, y, y);
							temp += y;
						}
						resized_y = temp/count_w;
						resample_image.setColor(i/count_w, j/count_h, resized_y,resized_y, resized_y);
						temp = 0;
					}
				}
			}
		}
		resample_image.save("resample.bmp");
	}

	//
	//	4. Quantization
	//
	Bitmap resample_image("resample.bmp");
	int resample_width = resample_image.getWidth();
	int resample_height = resample_image.getHeight();
	for (int i = 0; i < resample_width; i++) {
		for (int j = 0; j < resample_height; j++) {
			unsigned char y;
			resample_image.getColor(i, j, y, y, y);
			int temp = floor(y / 32);
			resample_image.setColor(i, j, temp, temp, temp);
		}
	}
	resample_image.save("resample.bmp");

	//
	//  5. ASCII Mapping and printout + Bonus (save as bitmap)
	//
	Bitmap shades_image(resample_width * 8, resample_height * 8);
	for (int i = 0; i < resample_height; i++) {
		for (int j = 0; j < resample_width; j++) {
			unsigned char y;
			resample_image.getColor(j, i, y, y, y);

            // flip the color for screen print
			int temp = y;
			if (*argv[1] == 's') {
				temp = 7 - y;
				resample_image.setColor(j, i, temp, temp, temp);
			}

            // find the corresponding bitmap for each shade
			char *path;
			switch (temp) {
				case 0: path = "shades/0.bmp";
					break;
				case 1: path = "shades/1.bmp";
					break;
				case 2: path = "shades/2.bmp";
					break;
				case 3: path = "shades/3.bmp";
					break;
				case 4: path = "shades/4.bmp";
					break;
				case 5: path = "shades/5.bmp";
					break;
				case 6: path = "shades/6.bmp";
					break;
				case 7: path = "shades/7.bmp";
					break;
				default: printf("Error: invalid shade\n");
					break;
			}

            // set the color for each pixel of the shade
			Bitmap shade(path);
			for (int s = 0; s < 8; s++)
				for (int t = 0; t < 8; t++) {
					unsigned char z;
					shade.getColor(t, s, z, z, z);
					shades_image.setColor(j * 8 + t, i * 8 + s, z, z, z);
				}
			shade.~Bitmap();

			char quantize = shades[temp];
			printf("%c", quantize);
		}
		printf("\n");
	}
	shades_image.save("shades.bmp");
    // save the flipped version of resampling
    resample_image.save("resample.bmp");
	
	//
	//  6. ASCII art txt file
	//
	if (argc > 4) {
		// extract the filename extension
		char FNE[3];
		strcpy(FNE, argv[5]+strlen(argv[5])-3);
		// check if the arguement belongs to txt
		if (strcmp(FNE, "txt") == 0) {
			FILE *output = fopen(argv[5], "w");
			for (int i = 0; i < resample_height; i++) {
				for (int j = 0; j < resample_width; j++) {
					unsigned char y;
					resample_image.getColor(j, i, y, y, y);
					char quantize = shades[y];
					fprintf(output, "%c", quantize);
				}
				fprintf(output, "\n");
			}
			fclose(output);
		}
		else printf("Expected txt output file in argv[5]\n");
	}

	//
	// Bonus (output HTML file)
	//
	if (argc > 5) {
		// extract the filename extension
		char FNE[4];
		strcpy(FNE, argv[6]+strlen(argv[6])-4);
		// check if the arguement belongs to html
		if (strcmp(FNE, "html") == 0) {
			FILE *HTML = fopen(argv[6], "w");
			fprintf(HTML, "<HTML><body style=\"font-family: monospace; font-size: 1; line-height: 5%%;\">\n");
			for (int i = 0; i < resample_height; i++) {
				fprintf(HTML, "<pre>");
				for (int j = 0; j < resample_width; j++) {
					unsigned char y;
					resample_image.getColor(j, i, y, y, y);
					// base value = y * 32; deviation = y * 4.5
					y = y * 32 + floor(y * 4.5);
					fprintf(HTML, "<strong style=\"color: rgb(%d, %d, %d)\">&#10042;</strong>", y, y, y);
				}
				fprintf(HTML, "</pre>");
			}
			fprintf(HTML, "</body></HTML>");
			fclose(HTML);
		}
		else printf("Expected html output file in argv[6]\n");
	}
	
	//  free memory
	image_data.~Bitmap();
	bw_image.~Bitmap();
	resample_image.~Bitmap();
	shades_image.~Bitmap();

	return 0;
} 