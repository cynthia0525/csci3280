	CSCI3280 Assignment 1 ASCII Art

Name: CHAN Ka Yi
Student ID: 1155093641

Points to note: 
	// Standard
	ascii <s|p> <input.bmp> <width> <height> <*output.txt>

	// Bonus
	ascii_bonus <s|p> <input.bmp> <width> <height> <*output.txt> <*output.html>
	items with * mark are optional arguements

	shades.bmp  :	a width * 8 x height * 8 bmp image will be generated in each execution
	output.html :	a simple HTML file (self-defined output name) of the ASCII art will be generated on demand

	To correctly generate shades.bmp, the 'shades' folder (provided in the source zip) has to be included in the exe directory.
	Some temporary files including b&w.bmp and resample.bmp will be generated in each execution.
	Due to the hardly recognizable image generated, 8 levels (instead of 256 levels) of colors are used in HTML output.
	It takes a little time to complete all the exports, especially for shades.bmp, so please be patient.