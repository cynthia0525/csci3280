/*

CSCI 3280, Introduction to Multimedia Systems
Spring 2019

Assignment 01 Skeleton

ascii.cpp

*/

#include "stdio.h"
#include "stdlib.h"
#include "malloc.h"
#include "memory.h"
#include "math.h"
#include "bmp.h"		//	Simple .bmp library

#define MAX_SHADES 8

char shades[MAX_SHADES] = {'@','#','%','*','|','-','.',' '};

#define SAFE_FREE(p)  { if(p){ free(p);  (p)=NULL;} }


int main(int argc, char** argv)
{
	//
	//	1. Open BMP file
	//
	Bitmap image_data(argv[2]);

	if(image_data.getData() == NULL) {
		printf("unable to load bmp image!\n");
		return -1;
	}

	int width = image_data.getWidth();
	int height = image_data.getHeight();

	//
	//	2. Obtain Luminance
	//
	Bitmap bw_image(width, height);
	for (int i = 0; i < width; i++) {
		for (int j = 0; j < height; j++) {
			unsigned char r, g, b, y;
			image_data.getColor(i, j, r, g, b);
			y = r * 0.299 + g * 0.587 + b * 0.114;
			bw_image.setColor(i, j, y, y, y);
		}
	}
	bw_image.save("b&w.bmp");

	//
	//  3. Resize image
	//
	if (argc > 2) {
		int resample_width = atoi(argv[3]);
		int resample_height = atoi(argv[4]);

		// resize w/o deformation
		if (resample_width == 0 && resample_height == 0) {
			resample_width = width;
			resample_height = height;
		}
		else if (resample_width == 0 && resample_height != 0)
			resample_width = resample_height;
		else if (resample_width != 0 && resample_height == 0)
			resample_height = resample_width;
		
		// declare the resize ratio of each side
		int count_w = 1;
		while (width/count_w > resample_width)
			count_w++;
		int count_h = 1;
		while (height/count_h > resample_height)
			count_h++;
        // keep the same ratio if deformation is not applicable
		if (atoi(argv[3]) == 0 || atoi(argv[4]) == 0) {
			if (count_w < count_h)
				count_w = count_h;
			else count_h = count_w;
		}
        // redefine the width and height of the resized image
		resample_width = width/count_w;
		resample_height = height/count_h;

		Bitmap resample_image(resample_width, resample_height);
		for (int i = 0; i < width; i++) {
			int temp = 0;
			for (int j = 0; j < height; j++) {
				unsigned char y;
				bw_image.getColor(i, j, y, y, y);
				temp += y;
				// resample height
				if ((j+1)%count_h == 0) {
					unsigned char resized_y = temp/count_h;
					bw_image.setColor(i, j/count_h, resized_y, resized_y, resized_y);
					temp = 0;
					// resample width
					if ((i+1)%count_w == 0) {
						for (int k = 0; k < count_w; k++) {
							bw_image.getColor(i-k, j/count_h, y, y, y);
							temp += y;
						}
						resized_y = temp/count_w;
						resample_image.setColor(i/count_w, j/count_h, resized_y,resized_y, resized_y);
						temp = 0;
					}
				}
			}
		}
		resample_image.save("resample.bmp");
	}

	//
	//	4. Quantization
	//
	Bitmap resample_image("resample.bmp");
	int resample_width = resample_image.getWidth();
	int resample_height = resample_image.getHeight();
	for (int i = 0; i < resample_width; i++) {
		for (int j = 0; j < resample_height; j++) {
			unsigned char y;
			resample_image.getColor(i, j, y, y, y);
			int temp = floor(y / 32);
			resample_image.setColor(i, j, temp, temp, temp);
		}
	}
	resample_image.save("resample.bmp");

	//
	//  5. ASCII Mapping and printout
	//
	for (int i = 0; i < resample_height; i++) {
		for (int j = 0; j < resample_width; j++) {
			unsigned char y;
			resample_image.getColor(j, i, y, y, y);

            // flip the color for screen print
			int temp = y;
			if (*argv[1] == 's') {
				temp = 7 - y;
				resample_image.setColor(j, i, temp, temp, temp);
			}
			
			char quantize = shades[temp];
			printf("%c", quantize);
		}
		printf("\n");
	}
    // save the flipped version of resampling
    resample_image.save("resample.bmp");
	
	//
	//  6. ASCII art txt file
	//
	if (argc > 4) {
		FILE *output = fopen(argv[5], "w");
		for (int i = 0; i < resample_height; i++) {
			for (int j = 0; j < resample_width; j++) {
				unsigned char y;
				resample_image.getColor(j, i, y, y, y);
				char quantize = shades[y];
				fprintf(output, "%c", quantize);
			}
			fprintf(output, "\n");
		}
		fclose(output);
	}
	
	//  free memory
	image_data.~Bitmap();
	bw_image.~Bitmap();
	resample_image.~Bitmap();

	return 0;
} 